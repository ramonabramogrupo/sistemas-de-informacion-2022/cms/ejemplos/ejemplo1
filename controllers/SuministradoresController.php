<?php

namespace app\controllers;

use app\models\S;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SuministradoresController implements the CRUD actions for S model.
 */
class SuministradoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all S models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => S::find(),
            
            'pagination' => [
                'pageSize' => 5
            ],
            'sort' => [
                'defaultOrder' => [
                    's' => SORT_DESC,
                ]
            ],
            
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single S model.
     * @param string $s S
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($s)
    {
        return $this->render('view', [
            'model' => $this->findModel($s),
        ]);
    }

    /**
     * Creates a new S model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new S();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 's' => $model->s]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing S model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $s S
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($s)
    {
        $model = $this->findModel($s);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 's' => $model->s]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing S model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $s S
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($s)
    {
        $this->findModel($s)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the S model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $s S
     * @return S the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($s)
    {
        if (($model = S::findOne(['s' => $s])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace app\controllers;

use app\models\Spj;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SpjController implements the CRUD actions for Spj model.
 */
class SpjController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Spj models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Spj::find(),
            
            'pagination' => [
                'pageSize' => 5
            ],
            'sort' => [
                'defaultOrder' => [
                    's' => SORT_DESC,
                    'p' => SORT_DESC,
                    'j' => SORT_DESC,
                ]
            ],
            
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Spj model.
     * @param string $s S
     * @param string $p P
     * @param string $j J
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($s, $p, $j)
    {
        return $this->render('view', [
            'model' => $this->findModel($s, $p, $j),
        ]);
    }

    /**
     * Creates a new Spj model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Spj();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 's' => $model->s, 'p' => $model->p, 'j' => $model->j]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Spj model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $s S
     * @param string $p P
     * @param string $j J
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($s, $p, $j)
    {
        $model = $this->findModel($s, $p, $j);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 's' => $model->s, 'p' => $model->p, 'j' => $model->j]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Spj model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $s S
     * @param string $p P
     * @param string $j J
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($s, $p, $j)
    {
        $this->findModel($s, $p, $j)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Spj model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $s S
     * @param string $p P
     * @param string $j J
     * @return Spj the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($s, $p, $j)
    {
        if (($model = Spj::findOne(['s' => $s, 'p' => $p, 'j' => $j])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

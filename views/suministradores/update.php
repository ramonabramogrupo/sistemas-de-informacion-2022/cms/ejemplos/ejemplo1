<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\S $model */

$this->title = 'Actualizar Suministrador: ' . $model->s;
$this->params['breadcrumbs'][] = ['label' => 'Suministradores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->s, 'url' => ['view', 's' => $model->s]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="s-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

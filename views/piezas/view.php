<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\P $model */

$this->title = "Detalles de la pieza: " . $model->P; // cambiar el titulo de la web
$this->params['breadcrumbs'][] = [
                                    'label' => 'Piezas' , // cambiando la etiqueta de las migas  
                                    'url' => ['index']
                                ];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="p-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
                    'Actualizar',  // texto del boton
                    ['update', 'P' => $model->P],  // accion del controlador
                    ['class' => 'btn btn-primary'] // estilo visual del boton
                ) ?>
        <?= Html::a(
                'Eliminar',  // texto del boton
                ['delete', 'P' => $model->P],  // accion del controlador
                [
                    'class' => 'btn btn-danger', // estilo visual
                    'data' => [
                        'confirm' => '¿Seguro que quieres eliminar la pieza?', // mensaje confirmacion
                        'method' => 'post',
                    ],
                ]
            ) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'P',
            'nomp',
            'color',
            'peso',
            'ciudad',
            'piezasPorColor',
            'piezasPorCiudad',
        ],
    ]) ?>

</div>

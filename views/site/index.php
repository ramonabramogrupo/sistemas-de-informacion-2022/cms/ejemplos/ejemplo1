<?php

/* @var $this yii\web\View */

$this->title = 'Ejemplo1 Sistemas';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Ejemplo1 de Sistemas</h1>
        
        <p class="lead">Gestion de Piezas y Suministros</p>
    </div>
    <div>
        <?php
            echo yii\helpers\Html::img(
                    "@web/imgs/logo.png", // ruta hasta la imagen
                    ["class"=>"img-thumbnail d-block mx-auto"] // colocar la imagen responsive y centrada
                );
        ?>
    </div>
    
   
</div>

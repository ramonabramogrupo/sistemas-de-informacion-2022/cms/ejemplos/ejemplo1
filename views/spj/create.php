<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Spj $model */

$this->title = 'Crear Suministro';
$this->params['breadcrumbs'][] = ['label' => 'Suministros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spj-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

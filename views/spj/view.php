<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Spj $model */

$this->title = "Detalles del Suministro " ;
$this->params['breadcrumbs'][] = ['label' => 'Suministros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="spj-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
                    'Actualizar', 
                    ['update', 's' => $model->s, 'p' => $model->p, 'j' => $model->j], 
                    ['class' => 'btn btn-primary']
                ) ?>
        <?= Html::a(
                    'Borrar', 
                    ['delete', 's' => $model->s, 'p' => $model->p, 'j' => $model->j], 
                    [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => '¿Estas seguro que desaeas eliminar este elemento?',
                            'method' => 'post',
                        ],
                    ]
                ) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            's',
            'p',
            'j',
            'cant',
        ],
    ]) ?>

</div>
